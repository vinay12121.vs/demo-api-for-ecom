import express from 'express'
import userModel from '../user/user-model'
import userRoutes from '../user/user-routes'
let routes = express.Router()
routes.use('/user', userRoutes)


routes.use((req, res, next) => {
  console.log("not found")
  res.json({status: "Not Found"})
})

module.exports = routes;