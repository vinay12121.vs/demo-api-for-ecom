import model from './user-model'

let userDetails =  (req, res, next) => {  
  let userData = new model.userModel({
    username: req.body.username,
    email: req.body.email,    
    password: req.body.password, 
    contact: req.body.contact
  })
  userData.save().then(result => {
    console.log(req.body.address)
    if(!result.address)
    return model.addressModel({
      address: req.body.address,
      userId: result._id
    })
    .save()
    else {
      return model.addressModel.findByIdAndUpdate(result.address, 
        {$push: {address: req.body.address}}
    ).save()
    }
  })
  .then(result => {
    console.log(`User Credentials Saved Successfully!!! \n ${result}`)
    res.status(200).json({
      Status: "User Details Added Successfully",
      result
    })
  })
  .catch(err => {
    console.log(`Error While Saving user Details \n ${err}`)
    res.status(500).json({
      Status: "Error While Saving user Details",
      err
    })
  })
}

let userUploads = (req, res, next) => {
  model.userModel.findOne({email: req.body.email})
  .then(result => {
    result.image = "http://localhost:4000/" + req.file.filename
    console.log(req.file)
    return result.save()
  })
  .then((result)=> {
    console.log("image saved Successfully")
    res.status(200).json({
      Status: "image saved successfully!!!",
      result
    })
  })
  .catch(err => {
    console.log("error while saving image")
    res.status(500).json({
      status: "Error while saving image",
      err
    })
  })
}


let updateUser = (req, res, next) => {
  model.userModel.findOne({email: req.body.email})
  .then(result => {
    console.log(req.body)
    result.username = req.body.username?req.body.username:result.username,
    result.contact = req.body.contact?req.body.contact:result.contact
    return result.save()
  })
  .then((result)=> {
    if(req.body.address!==undefined){
      console.log(`in defined!! \n ${result}`)
       return model.addressModel.findOneAndUpdate({userId: result._id}, 
        {$push: {address: req.body.address}},
        {safe: true, upsert: true},
    )
    }else {
      console.log("in undefined")
      return model.addressModel({
        address: req.body.address,
      }).save()      
    }
  })
  .then(result => {
    console.log("user updated Successfully")
    res.status(200).json({
      Status: "user updated successfully!!!"
    })
  })
  .catch(err => {
    console.log("error while updating data ", err)
    res.status(500).json({
      status: "Error while updating data",
      err
    })
  })
}

let userContact = (req, res, next) => {
  model.userModel.find()
  .then(result => {
    let count = 0
    let aggregatedResult = {}
    result.forEach(item => {
      count++
      model.userModel.findOneAndUpdate({_id : item._id}, 
      {contact:  req.body.contact[count-1]}, { runValidators: true })
      .then(result => {
        // console.log(`contact on ${count} position is saved ${aggregatedResult} \n ${result}`)
        aggregatedResult.obj = result
        console.log(aggregatedResult)
      })
      .catch(err => {
        console.log(err)
        res.json()
      })
    })
    console.log(aggregatedResult)
    res.json({
      Status: "Contact updated Successfully!!",
      aggregatedResult
    })
  })
  .catch(err => {
    console.log(err)
    res.json({
      Status: "Error while updating contact numbers",
      err})
  })
}


exports.userUploads = userUploads
exports.userDetails = userDetails
exports.updateUser = updateUser
exports.userContact = userContact