import express from 'express'
import env from 'dotenv'
import routes from './routes/routes'
import config from './config/config'

let app = express()
app.use(express.static('public'))
env.load()
app.use(routes)

let port = process.env.PORT || 3000
app.listen(port, () => console.log(`listening at port ${port}`))

