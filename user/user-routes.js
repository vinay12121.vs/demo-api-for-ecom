import express from 'express'
import model from './user-model'
import controller from './user-controller'
import bodyParser from 'body-parser'
import multer from 'multer'
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public')
  },
  filename : (req, file, cb) => {
    cb(null, new Date().toISOString() + file.originalname) 
  }
})
let routes = express.Router()
let upload = multer({storage: storage})
routes.use(bodyParser.urlencoded({extended: false}))
routes.use(bodyParser.json())
// routes.get('/user', useruser)
routes.post('/insert-datails', controller.userDetails)
routes.post('/upload-image', upload.single('productImage'), controller.userUploads)
routes.post('/update', controller.updateUser)
routes.post('/assign-contact', controller.userContact)

module.exports = routes