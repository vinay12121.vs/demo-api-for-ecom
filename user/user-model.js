import mongoose, { version } from 'mongoose'
let userSchema = mongoose.Schema({
  id: mongoose.Schema.Types.ObjectId,
  username: {type: String, unique: true, required: [true, "username required"]},
  email: {type: String, unique: true, index: true, required: [true, "email required"],
  validate: {
    validator: (value) => {
      return /[a-z0-9!@#$%^&.*()]{3,}@[a-z]{3,}\.com$/.test(value)
    },
    message: "Please provide valid email address"
  }
  },
  image: {type: String},
  password: {type: String, required: [true, "please provide a password"]},
  contact: {type: Number, unique: true, required: [true, "phone is required"],
  validate: {
    validator: (value) => {
      return /\d{10}/.test(value)
    },
    message: "Phone no. should contain 10 digits"
  }
},
})

let userModel = mongoose.model('userModel', userSchema)

let addressSchema = mongoose.Schema({  
  address: {type: [String], unique: true},
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref:"userModel"
  }
})
let addressModel = mongoose.model('addressModel', addressSchema)
exports.addressModel = addressModel
exports.userModel = userModel